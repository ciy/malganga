package com.malganga.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.malganga.controllers.ManagerController;


public class MainClass 
{	
	public static void main(String[] args) 
	{
		
		// Initialize application context 
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		// Take object of Controller to call methods
		ManagerController manager = (ManagerController) context.getBean(ManagerController.class);
		
		
		//System.out.println(manager.getBranches());
		
		System.out.println(manager.getManagerByBranchId(2));
		
		// manager can view list all branches
		//System.out.println(manager.getManagerByBranchId(1));

		
	
		// manager can search branch by its id
	//	System.out.println(manager.getBranchById(1));
		
		// manager can view total restaurants in given branch
		//System.out.println(manager.getRestaurantsByBranchId(1));
		
		// manager can add branch
		
		
		// manager can delete branch
		
			
		// can view its own data
		
		// manager can reset his password also forget password functionality available to him
		
		// manager can enter providers detail and register it in the system
		
		// manager can add restaurant / client data
		
		// manager can view restaurants list
		
		// manager can edit restaurant list
		
		// manager can add bearer data
		
		// manager can remove bearer
		
		// manager can add items
		
		// manager can delete items
		
		// manager can add stock data
		
		// manager can place order
		
		// manager can view order 
		
		// manager can generate bill
		
		// manager can send bill
		
		// manager can view orderDetail
		
		// manager can view reports 
		
		// close the context
		((AbstractApplicationContext) context).close();

	}

}
