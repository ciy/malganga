package com.malganga.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UtilSessionFactory 
{
	@Autowired
	SessionFactory sessionfactory;
	
	Session session;
	
	public Session getCurrentSession()
	{
		if(this.session == null)
			this.session = sessionfactory.openSession();
		else
			this.session = sessionfactory.getCurrentSession();
		return session;
	}
}
