package com.malganga.daos;

import java.util.List;

import com.malganga.entities.Bearer;
import com.malganga.entities.Branch;
import com.malganga.entities.Manager;
import com.malganga.entities.Provider;
import com.malganga.entities.Restaurant;

public interface BranchDao 
{
	// add new branch
	public int insert(Branch newBranch);

	// update branch head for particular branch
	public int update(int branchId,Manager manager);
	
	// get branch by BranchId
	public Branch getBranch(int branchId);
	
	// view list of all branches
	public List<Branch> select();

	// all managers in given branch
	public List<Manager> getManagers(int branchId);

	// all restaurants in given branch
	public List<Restaurant> getRestaurants(int branchId);

	// all providers
	public List<Provider> getProviders(int branchId);

	// all bearer
	public List<Bearer> getBearers(int branchId);
}
