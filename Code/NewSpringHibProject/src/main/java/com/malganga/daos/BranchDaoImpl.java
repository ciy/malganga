package com.malganga.daos;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.malganga.entities.Bearer;
import com.malganga.entities.Branch;
import com.malganga.entities.Manager;
import com.malganga.entities.Provider;
import com.malganga.entities.Restaurant;


@Repository
public class BranchDaoImpl implements BranchDao
{
	/* UtilSessionFactory is a utility class use to give single-tone object of session */
	@Autowired
	SessionFactory sessionFactory;

	public int insert(Branch newBranch)
	{
		// get current session
		Session session = sessionFactory.getCurrentSession();

		// insert data in branch table
		int rowsAffected = (Integer) session.save(newBranch);

		// return affected rows record in return
		return rowsAffected;
	}

	public int update(int branchId,Manager manager) 
	{
		// get current session
		Session session = sessionFactory.getCurrentSession();

		// get appropriate branch
		Branch branch = session.load(Branch.class, branchId);
		/* It should be replaced by load method as in web project the branch data will be first checked and the will be updated*/

		// update branch head
		branch.setManager(manager);

		// update the database
		session.persist(branch);

		// return 0 as success notation 
		return 0;
	}

	@SuppressWarnings("unchecked")
	public List<Branch> select() 
	{
		// get current session
		Session session = sessionFactory.getCurrentSession();

		// create query for select *
		Query query = session.createQuery("from Branch",Branch.class);

		// get list of branches and return it
		return query.getResultList();
	}

	public List<Manager> getManagers(int branchId) 
	{
		// get current session
		Session session = sessionFactory.getCurrentSession();

		// get branch by branch Id
		/* It should be replaced by load method as Branch data will be available in session */
		Branch branch = session.load(Branch.class , branchId);
			
			System.out.println(branch.getManagers().get(0).getContactPerson());
		// return managers
		return branch.getManagers();
	}

	public List<Restaurant> getRestaurants(int branchId) 
	{
		// get current session
		Session session = sessionFactory.getCurrentSession();

		// get branch by branch Id
		/* It should be replaced by load method as Branch data will be available in session */
		Branch branch = session.load(Branch.class , branchId);

		// return managers
		return branch.getRestaurants();
	}

	public List<Provider> getProviders(int branchId) {
		// get current session
		Session session = sessionFactory.getCurrentSession();

		// get branch by branch Id
		/* It should be replaced by load method as Branch data will be available in session */
		Branch branch = session.load(Branch.class , branchId);

		// return managers
		return branch.getProviders();
	}

	public List<Bearer> getBearers(int branchId) 
	{
		// get current session
		Session session = sessionFactory.getCurrentSession();

		// get branch by branch Id
		/* It should be replaced by load method as Branch data will be available in session */
		Branch branch = session.load(Branch.class , branchId);

		// return managers
		return branch.getBearers();
	}

	@Override
	public Branch getBranch(int branchId) 
	{
		// get session
		Session session = sessionFactory.getCurrentSession();
		
		// get Branch by primary key
		Branch branch = session.load(Branch.class , branchId);
		
		return branch;
	}

}
