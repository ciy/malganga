package com.malganga.services;

import java.util.List;

import com.malganga.entities.Bearer;
import com.malganga.entities.Branch;
import com.malganga.entities.Manager;
import com.malganga.entities.Provider;
import com.malganga.entities.Restaurant;

public interface BranchService 
{
	// add new branch
	public boolean addBranch(Branch newBranch);
	
	// update branch head for particular branch
	public boolean add_updateBranchHead(int branchId,Manager manager);
/*Please update the above functionality by passing Manager as argument [use OOPS for the same]*/
	
	// view list of all branches
	public List<Branch> listBranch();
	
	// get record of all managers working in particular branch
	public List<Manager> getManagers(int branchId);
	
	// get record of all restaurants under given branch
	public List<Restaurant> getRestaurants(int branchId);
	
	// list providers detail of these branch
	public List<Provider> getProviders(int branchId);
	
	// list bearer in the branch
	public List<Bearer> getBearers(int branchId);
	
	// get branch by Id
	public Branch getBranch(int branchId);
}