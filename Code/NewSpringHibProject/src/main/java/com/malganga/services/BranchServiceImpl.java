package com.malganga.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.malganga.daos.BranchDao;
import com.malganga.entities.Bearer;
import com.malganga.entities.Branch;
import com.malganga.entities.Manager;
import com.malganga.entities.Provider;
import com.malganga.entities.Restaurant;


@Transactional
@Service
public class BranchServiceImpl implements BranchService
{
	// create / autowire BranchDao object
	@Autowired
	BranchDao branchDao;
	
	public boolean addBranch(Branch newBranch) 
	{
		// if operation success insert branchDao method will return no of raws inserted
		if(branchDao.insert(newBranch)==1)		
			return true;
		return false;
	}

	public boolean add_updateBranchHead(int branchId, Manager manager) 
	{
		// update branch record to add branch head
				if(branchDao.update(branchId, manager)==1)
					return true;
				return false;		
	}
	
	public List<Branch> listBranch() 
	{
		// get list of branches from branchDao;
		return branchDao.select();
	}

	public List<Manager> getManagers(int branchId) {
		// return managers list 
		return branchDao.getManagers(branchId);
	}

	public List<Restaurant> getRestaurants(int branchId) {
		// restaurants list
		return branchDao.getRestaurants(branchId);
	}

	public List<Provider> getProviders(int branchId) {
		// Providers List
		return branchDao.getProviders(branchId);
	}

	public List<Bearer> getBearers(int branchId) {
		// bearers list
		return branchDao.getBearers(branchId);
	}

	@Override
	public Branch getBranch(int branchId) {
		// dao method
		return branchDao.getBranch(branchId);
	}
	
	
}
