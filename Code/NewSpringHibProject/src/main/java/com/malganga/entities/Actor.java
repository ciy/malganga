package com.malganga.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GeneratorType;



/*Annotate the class with @Entity; as it is going to be Entity for hibernate session
Note: @Entity is mandatory for every Entity in hibernate;
@Table is not compulsory; but as table is already created in database; to specify table properties @Table is used 
*/

@Entity
@Table(name="Actor")
public class Actor implements Serializable
{
	/*Attributes / Columns */
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="actor_id_pk")
	private int id;
	
	@Column(name="actor_name")
	private String name;
	
	@Column(name="actor_cperson")
	private String contactPerson;
	
	@Column(name="actor_role")
	private String role;
	
	@Column(name="actor_add")
	private String address;
	
	@Column(name="actor_country")
	private String pincode;
	
	@Column(name="actor_pincode")
	private String country;
	
	@Column(name="actor_mob1")
	private String mobile1;
	
	@Column(name="actor_mob2")
	private String mobile2;
	
	@Column(name="actor_email")
	private String email;
	
	@Column(name="actor_password")
	private String password;
	
	@ManyToOne(cascade= CascadeType.ALL)
	@JoinColumn(name="actor_branch_fk")
	private Branch branch;

	@OneToMany(mappedBy="manager")
	private List<Branch> branches;
	
	/*	Getters and Setters		*/
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMobile1() {
		return mobile1;
	}

	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}

	public String getMobile2() {
		return mobile2;
	}

	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	
	/*	Default c'tor	*/	
	public Actor() 
	{
		
	}

	
	
	/*	C'tor for Manager / Bearer */
	public Actor(String contactPerson, String address, String pincode, String country, String mobile1, String mobile2,
			String email, String password, Branch branch, String role) 
	{
		this.contactPerson = contactPerson;
		this.address = address;
		this.pincode = pincode;
		this.country = country;
		this.mobile1 = mobile1;
		this.mobile2 = mobile2;
		this.email = email;
		this.password = password;
		this.branch = branch;
		this.role	= role;
	}

	
	/*c'tor for Provider / Consumer 	*/	
	public Actor(String name, String contactPerson, String role, String address, String pincode, String country,
			String mobile1, String mobile2, String email, String password, Branch branch) 
	{
		this.name = name;
		this.contactPerson = contactPerson;
		this.role = role;
		this.address = address;
		this.pincode = pincode;
		this.country = country;
		this.mobile1 = mobile1;
		this.mobile2 = mobile2;
		this.email = email;
		this.password = password;
		this.branch = branch;
	}
	
	
}
