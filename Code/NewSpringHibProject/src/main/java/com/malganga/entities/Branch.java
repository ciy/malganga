package com.malganga.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Branch")
public class Branch implements Serializable
{
	@Id				/*	Identifier for table */
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="branch_id_pk")
	private int id;
	
	@Column(name="branch_add")
	private String address;
	
	/*	It will give branch head for given branch
	 * as Manager can be head of more than one branch it is mapped as ManyToOne*/ 
	@ManyToOne(cascade= CascadeType.ALL)
	@JoinColumn(name="branch_head_fk")	
	private Manager manager;


	/* to enlist bearer in given branch
	 * As many bearers will be working in particular branch mapping is OneTOMany 
	 * i.e. One Branch Many Bearers same for Manager,Provider,Restaurants
	 * */	
	@OneToMany(mappedBy="branch")
	private List<Bearer> bearers; 
	
	
	// list of manager in particular branch
	@OneToMany(mappedBy="branch")
	private List<Manager> managers; 
	
	
	// List of providers in given branch
	@OneToMany(mappedBy="branch")
	private List<Provider> providers; 
	
	// List of Restaurants
	@OneToMany(mappedBy="branch",fetch=FetchType.LAZY)
	private List<Restaurant> restaurants;
	
	

	public int getId() {
		return id;
	}

	public List<Bearer> getBearers() {
		return bearers;
	}

	public void setBearers(List<Bearer> bearers) {
		this.bearers = bearers;
	}

	public List<Manager> getManagers() {
		return managers;
	}

	public void setManagers(List<Manager> managers) {
		this.managers = managers;
	}

	public List<Provider> getProviders() {
		return providers;
	}

	public void setProviders(List<Provider> providers) {
		this.providers = providers;
	}

	public List<Restaurant> getRestaurants() {
		return restaurants;
	}

	public void setRestaurants(List<Restaurant> restaurants) {
		this.restaurants = restaurants;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	// default c'tor
	public Branch() 
	{
		
	}

	// c'tor  to add branch address
	public Branch(String address) {
		this.address = address;
	}

	
	// c'tor to update branch head
	public Branch(Manager manager) {
		this.manager = manager;
	}

	@Override
	public String toString() {
		return "Branch [id=" + id + ", address=" + address + ", branch head=" + manager.getContactPerson()+ "]\n";
	}

}
