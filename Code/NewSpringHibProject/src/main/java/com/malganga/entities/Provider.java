package com.malganga.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Provider")
public class Provider 
{
	@Id
	@Column(name="actor_id_pk")
	private int id;
	
	@Column(name="actor_name")
	private String companyName;
	
	@Column(name="actor_cperson")
	private String contactPerson;
	
	@Column(name="actor_role")
	private String role;
	
	@Column(name="actor_add")
	private String address;
	
	@Column(name="actor_country")
	private String pincode;
	
	@Column(name="actor_pincode")
	private String country;
	
	@Column(name="actor_mob1")
	private String mobile1;
	
	@Column(name="actor_mob2")
	private String mobile2;
	
	@Column(name="actor_email")
	private String email;
	
	@Column(name="actor_password")
	private String password;
	
	@ManyToOne(cascade= CascadeType.ALL)
	@JoinColumn(name="actor_branch_fk")
	private Branch branch;

	
	// default c'tor
	public Provider() 
	{
		
	}

	// c'tor for manager registration
	public Provider(String contactPerson, String address, String pincode, String country, String mobile1, String mobile2,
			String email, String password) {
		this.role="provider";
		this.contactPerson = contactPerson;
		this.address = address;
		this.pincode = pincode;
		this.country = country;
		this.mobile1 = mobile1;
		this.mobile2 = mobile2;
		this.email = email;
		this.password = password;
	}

	
	//	c'tor for manager registration by manager
	public Provider(String companyName, String contactPerson, String address, String pincode,
			String country, String mobile1, String mobile2, String email, String password, Branch branch) {
		super();
		this.companyName = companyName;
		this.contactPerson = contactPerson;
		this.role="provider";
		this.address = address;
		this.pincode = pincode;
		this.country = country;
		this.mobile1 = mobile1;
		this.mobile2 = mobile2;
		this.email = email;
		this.password = password;
		this.branch = branch;
	}
	// setters
	public void setId(int id) {
		this.id = id;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}

	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	//getters
	public int getId() {
		return id;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public String getRole() {
		return role;
	}

	public String getAddress() {
		return address;
	}

	public String getPincode() {
		return pincode;
	}

	public String getCountry() {
		return country;
	}

	public String getMobile1() {
		return mobile1;
	}

	public String getMobile2() {
		return mobile2;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public Branch getBranch() {
		return branch;
	}

	
	public String getCompanyName() {
		return companyName;
	}

	@Override
	public String toString() {
		return "Manager [id=" + id + ", contactPerson=" + contactPerson + ", role=" + role + ", address=" + address
				+ ", pincode=" + pincode + ", country=" + country + ", mobile1=" + mobile1 + ", mobile2=" + mobile2
				+ ", email=" + email + ", branch=" + branch.getAddress() + "]\n";
	}
	
		

}
