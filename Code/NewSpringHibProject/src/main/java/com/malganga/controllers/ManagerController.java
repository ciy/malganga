package com.malganga.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.malganga.entities.Bearer;
import com.malganga.entities.Branch;
import com.malganga.entities.Manager;
import com.malganga.entities.Provider;
import com.malganga.entities.Restaurant;
import com.malganga.services.BranchService;


// mark the class with @component / @Controller for component scanning
@Controller
public class ManagerController 
{
	// for branch related operations
	@Autowired	
	BranchService branchService;

	
	// list all of the branches
	
	public List<Branch> getBranches()
	{
		return branchService.listBranch();
	}
	
	
	// list of Restaurants for particular branch
	public List<Restaurant> getRestaurantsByBranchId(int branchId)
	{
		return branchService.getRestaurants(branchId);
	}
	
	// list of Restaurants for particular branch
	public List<Provider> getProvidersByBranchId(int branchId)
	{
		return branchService.getProviders(branchId);
	}
	
	// list of Restaurants for particular branch
	public List<Bearer> getBearersByBranchId(int branchId)
	{
		return branchService.getBearers(branchId);
	}
	
	// list of Restaurants for particular branch
	public List<Manager> getManagerByBranchId(int branchId)
	{
		return branchService.getManagers(branchId);
	}
	
	// get branch detail
	public Branch getBranchById(int branchId)
	{
		return branchService.getBranch(branchId);
	}
}
