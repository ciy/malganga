package com.malganga.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerController 
{
	private String status="success";
	
	@Autowired
	private DemoController demo;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public DemoController getDemo() {
		return demo;
	}

	public void setDemo(DemoController demo) {
		this.demo = demo;
	}

	@Override
	public String toString() {
		return "CustomerController [status=" + status + ", demo=" + demo + "]";
	}
	
}
