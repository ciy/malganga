package com.malganga.controllers;

import org.springframework.stereotype.Component;

@Component
public class DemoController 
{
	private String status = "success";

	public String getStatus() 
	{
		return status;
	}

	public void setStatus(String status) 
	{
		this.status = status;
	}

	
	@Override
	public String toString() {
		return "DemoController [status=" + status + "]";
	}
		
}
