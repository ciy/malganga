package com.malganga.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


import com.malganga.controllers.CustomerController;

public class MainClass {

	public static void main(String[] args) 
	{
		ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
		
		CustomerController objController = (CustomerController) context.getBean(CustomerController.class);
		
		System.out.println(objController);
		
	}

}
