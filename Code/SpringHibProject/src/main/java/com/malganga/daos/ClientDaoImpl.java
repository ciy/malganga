package com.malganga.daos;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.malganga.entities.Client;
import com.malganga.utils.UtilSessionFactory;


@Repository
public class ClientDaoImpl implements ClientDao 
{
		
	@Autowired
	UtilSessionFactory mySessionFactory;

	Session session;

	
	@SuppressWarnings("unchecked")
	public List<Client> select()
	{	
		session = mySessionFactory.getCurrentSession();
		Query query = session.createQuery("from Client");
		return query.getResultList();
	}



	

}
