package com.malganga.daos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.malganga.entities.BranchM;
import com.malganga.entities.Client;
import com.malganga.entities.Employee;
import com.malganga.utils.UtilSessionFactory;

@Repository
public class BranchDaoImpl implements BranchDao 
{
	@Autowired
	UtilSessionFactory sessionFactory;
	
	public List<BranchM> select()
	{
		Session session = sessionFactory.getCurrentSession();
		
		Query query=session.createQuery("from BranchM");
		
		return query.getResultList();
	}
	
	public List<Employee> getAllEmp(int branchId)
	{
		Session session = sessionFactory.getCurrentSession();
		
		BranchM branch =session.get(BranchM.class, branchId);
		
		return branch.getEmployees();
	}
	
	public List<Client> getAllClient(int branchId)
	{
		Session session = sessionFactory.getCurrentSession();
		
		BranchM branch =session.get(BranchM.class, branchId);
		
		return branch.getClients();
	}
}
