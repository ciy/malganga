package com.malganga.daos;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.malganga.entities.Employee;
import com.malganga.utils.UtilSessionFactory;


@Repository
public class EmployeeDaoImpl implements EmployeeDao 
{
		
	@Autowired
	UtilSessionFactory mySessionFactory;

	Session session;

	// Select all query on Employee
	@SuppressWarnings("unchecked")
	public List<Employee> select()
	{
		/*		
		try 
		{
			//sessionFactory.openSession();
			session = sessionFactory.getCurrentSession();
			System.out.println("getting current session.....");
		}
		catch (Exception e) 
		{
			session = sessionFactory.openSession();
			System.out.println("Not able to get session... ");
		}
		*/
		
		session = mySessionFactory.getCurrentSession();
		Query query = session.createQuery("from Employee");
		return query.getResultList();
	}



	

}
