package com.malganga.daos;

import java.util.List;

import com.malganga.entities.Client;

public interface ClientDao {

	// Select all query on Client
	List<Client> select();

}