package com.malganga.daos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.malganga.entities.Item;
import com.malganga.utils.UtilSessionFactory;

@Repository
public class ItemDaoImpl implements ItemDao 
{
	@Autowired
	UtilSessionFactory sessionfactory;
	
	public List<Item> select()
	{
		Session session = sessionfactory.getCurrentSession();
		
		Query query=session.createQuery("from Item");
		
		return query.getResultList();
	}
}
