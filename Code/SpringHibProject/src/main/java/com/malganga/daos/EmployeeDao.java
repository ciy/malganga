package com.malganga.daos;

import java.util.List;

import com.malganga.entities.Employee;

public interface EmployeeDao {

	// Select all query on Employee
	List<Employee> select();

}