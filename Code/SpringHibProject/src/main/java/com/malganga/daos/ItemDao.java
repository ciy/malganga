package com.malganga.daos;

import java.util.List;

import com.malganga.entities.Item;

public interface ItemDao {

	List<Item> select();

}