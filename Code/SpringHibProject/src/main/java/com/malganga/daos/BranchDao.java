package com.malganga.daos;

import java.util.List;

import com.malganga.entities.BranchM;
import com.malganga.entities.Client;
import com.malganga.entities.Employee;

public interface BranchDao 
{
	List<BranchM> select();
	public List<Employee> getAllEmp(int branchId);
	public List<Client> getAllClient(int branchId);
}