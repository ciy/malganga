package com.malganga.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.malganga.controllers.CustomerController;

public class MainClass {

	/*
	 * @Autowired
	staticprivate CustomerController customer; 
	First solution to inject CustomerController object is by using @Autowire anotation
	*/
	
	public static void main(String[] args) 
	{
		
		// Initialize application context 
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		// Take object of Controller to call methods
		CustomerController customer = (CustomerController) context.getBean(CustomerController.class);
		
		/*
		 * @Autowired 
		CustomerController customer;
		we can use @Autowire outside any method ;here since we are injecting customer inside static method ;I have injected it using proper method;
		*/
		
		/*System.out.println(customer.getEmployee());*/
		
		/*System.out.println(customer.getBranches());*/
		
		/*System.out.println(customer.getAllEmp(1));*/
		
		/*System.out.println(customer.getClients());*/
		
		/*System.out.println(customer.getAllClient(1));*/
		
		System.out.println(customer.getItems());

	}

}
