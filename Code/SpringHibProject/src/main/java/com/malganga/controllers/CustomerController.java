package com.malganga.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.malganga.entities.BranchM;
import com.malganga.entities.Client;
import com.malganga.entities.Employee;
import com.malganga.entities.Item;
import com.malganga.services.BranchService;
import com.malganga.services.ClientService;
import com.malganga.services.EmployeeService;
import com.malganga.services.ItemService;

// mark the class with @component / @Controller for component scanning
@Controller
public class CustomerController 
{
	@Autowired
	EmployeeService emp;
	
	@Autowired
	BranchService branch;
	
	@Autowired
	ClientService client;
	
	@Autowired
	ItemService item;
	
	//call method to view employees
	public List<Employee> getEmployee()
	{
		return emp.getEmployee();
	}
	
	public List<BranchM> getBranches()
	{
		return branch.getBranches();
	}
	
	public List<Client> getClients()
	{
		return client.getClients();
	}
	
	public List<Employee> getAllEmp(int branchId)
	{
		return branch.getAllEmp(branchId);
	}
	
	public List<Client> getAllClient(int branchId)
	{
		return branch.getAllClient(branchId);
	}
	
	public List<Item> getItems()
	{
		return item.getItems();
	}
	
	
	/*for testing*/
	@Override
	public String toString() 
	{
		return "CustomerController [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
}
