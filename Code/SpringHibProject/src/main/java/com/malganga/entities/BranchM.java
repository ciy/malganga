package com.malganga.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="branch")
public class BranchM implements Serializable
{
	@Id				/*	Identifier for table */
	@Column(name="branch_id_pk")
	private int id;
	
	@Column(name="branch_add")
	private String address;
	
	/*verison 1:*/
	
	@Column(name="branch_head_fk")
	private String branchHead;
	

	@OneToMany(mappedBy="branch", cascade=CascadeType.ALL)
	private List<Employee> employees;
	
	@OneToMany(mappedBy="branch", cascade=CascadeType.ALL)
	private List<Client> clients;
	
	
	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	@Override
	public String toString() {
		return "BranchM [id=" + id + ", address=" + address + ", branchHead=" + branchHead + "]";
	}

	public BranchM(int id, String address, String branchHead, List<Employee> employees) {
		super();
		this.id = id;
		this.address = address;
		this.branchHead = branchHead;
		this.employees = employees;
	}

	/* version2
	 *  As branch head is a employee it is mapped with Employee entity and
	 * every branch has one branch head hence mapping is one to one
	 * here,
	 * branch_head_fk is a column of branch table which will be mapped as a foreign key to get value of employee table
	 */
	/*
	 * version3:
	 * To avoid looping condition;
	 * temporarily one To one mapping is removed ...
	 * suggest changes if any 
	 * @OneToOne()
	@JoinColumn(name="branch_head_fk")
	private Employee employee;*/
	
	
	/*
	 * Many employees works in a single branch ;
	 * so to find out detail about the employees in the branch one to many mapping is used.
	 * here emp_branch_fk is a column of employee table through which many employees are mapped to a particular branch
	 */
	
	public String getBranchHead() {
		return branchHead;
	}

	public void setBranchHead(String branchHead) {
		this.branchHead = branchHead;
	}
	
	
	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}


	public int getId() {
		return id;
	}
	
	public String getAddress() {
		return address;
	}
	

	public void setId(int id) {
		this.id = id;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	public BranchM() 
	{
		
	}

	public BranchM(int id, String address) 
	{
		this.id = id;
		this.address = address;
		
	}
	
}
