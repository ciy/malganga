package com.malganga.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



/*Annotate the class with @Entity; as it is going to be Entity for hibernate session
Note: @Entity is mandatory for every Entity in hibernate;
@Table is not compulsory; but as table is already created in database; to specify table properties @Table is used 
*/
@Entity
@Table(name="employee")
public class Employee implements Serializable
{
	@Id
	@Column(name="emp_id_pk")
	private String empid;
	
	@Column(name="emp_name")
	private String name;
	
	@Column(name="emp_role")
	private String role;
	 
	@Column(name="emp_password")
	private String password;
	
	@Column(name="emp_mobile1")
	private String mobile1;
	
	@Column(name="emp_mobile2")
	private String mobile2;
	
	@Column(name="emp_email")
	private String email;
	
	/*@Column(name="emp_branch_fk")
	private int branch;*/
	
	@ManyToOne(cascade= CascadeType.ALL)
	@JoinColumn(name="emp_branch_fk")
	private BranchM branch;
	
	/*
	@Column is used to specify the properties of column for a given table;
	we can use this annotation on class variables(attribute) also but ideally it should get use on getters;
	*/
	
	/*	Getters	*/
	
	/*	Attributes/Fields	*/
	
	public String getEmpid() {
		return empid;
	}

	public String getName() {
		return name;
	}

	public String getRole() {
		return role;
	}

	public String getPassword() {
		return password;
	}

	public String getMobile1() {
		return mobile1;
	}

	public String getMobile2() {
		return mobile2;
	}

	public String getEmail() {
		return email;
	}
/*
	public int getBranch() {
		return branch;
	}
	*/
	
	/*	Setters	*/
	public void setEmpid(String empid) {
		this.empid = empid;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public void setRole(String role) {
		this.role = role;
	}
	
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}
	
	
	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}
	
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	/*public void setBranch(int branch) {
		this.branch = branch;
	}*/

		/*	Parameterless c'tor	:It should be present for bean initialization */
	public Employee() 
	{
	
	}
	
	
public BranchM getBranch() {
		return branch;
	}

	public void setBranch(BranchM branch) {
		this.branch = branch;
	}

	/*	Parameterized c'tor	*/			
	public Employee(String empid, String name, String role, String password, String mobile1, String mobile2,
			String email, BranchM branch) {
		super();
		this.empid = empid;
		this.name = name;
		this.role = role;
		this.password = password;
		this.mobile1 = mobile1;
		this.mobile2 = mobile2;
		this.email = email;
		this.branch = branch;
	}

	/*	Parameterized for testing purpose	*/
	public Employee(String empid, String name, String role, String password, String mobile1, String mobile2,
			String email) {
		super();
		this.empid = empid;
		this.name = name;
		this.role = role;
		this.password = password;
		this.mobile1 = mobile1;
		this.mobile2 = mobile2;
		this.email = email;
	}

	@Override
	public String toString() {
		return "Employee [empid=" + empid + ", name=" + name + ", role=" + role + ", password=" + password
				+ ", mobile1=" + mobile1 + ", mobile2=" + mobile2 + ", email=" + email + ", branch=" + branch + "]";
	}

}
