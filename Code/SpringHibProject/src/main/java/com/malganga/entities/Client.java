package com.malganga.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



/*Annotate the class with @Entity; as it is going to be Entity for hibernate session
Note: @Entity is mandatory for every Entity in hibernate;
@Table is not compulsory; but as table is already created in database; to specify table properties @Table is used 
*/

@Entity
@Table(name="client")
public class Client implements Serializable
{
	@Id
	@Column(name="client_id_pk")
	private int id;
	
	@Column(name="client_name")
	private String name;
	
	@Column(name="client_cperson")
	private String contactPerson;
	
	@Column(name="client_role")
	private String role;
	
	@Column(name="client_add")
	private String address;
	
	@Column(name="client_pincode")
	private String pincode;
	
	@Column(name="client_country")
	private String country;
	
	@Column(name="client_mob1")
	private String mobile1;
	
	@Column(name="client_mob2")
	private String mobile2;
	
	@Column(name="client_email")
	private String email;
	
	@Column(name="client_password")
	private String password;
	
	@ManyToOne(cascade= CascadeType.ALL)
	@JoinColumn(name="client_branch_fk")
	private BranchM branch;

	public Client() 
	{
	}

	public Client(int id, String name, String contactPerson, String role, String address, String pincode,
			String country, String mobile1, String mobile2, String email, String password, BranchM branch) {
		this.id = id;
		this.name = name;
		this.contactPerson = contactPerson;
		this.role = role;
		this.address = address;
		this.pincode = pincode;
		this.country = country;
		this.mobile1 = mobile1;
		this.mobile2 = mobile2;
		this.email = email;
		this.password = password;
		this.branch = branch;
	}

	
	//for testing purpose
	public Client(String name, String role) {
		super();
		this.name = name;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMobile1() {
		return mobile1;
	}

	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}

	public String getMobile2() {
		return mobile2;
	}

	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public BranchM getBranch() {
		return branch;
	}

	public void setBranch(BranchM branch) {
		this.branch = branch;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", name=" + name + ", contactPerson=" + contactPerson + ", role=" + role
				+ ", address=" + address + ", pincode=" + pincode + ", country=" + country + ", mobile1=" + mobile1
				+ ", mobile2=" + mobile2 + ", email=" + email + ", password=" + password + ", branch=" + branch + "]";
	}

}
