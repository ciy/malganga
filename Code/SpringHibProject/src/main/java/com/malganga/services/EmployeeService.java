package com.malganga.services;

import java.util.List;

import com.malganga.entities.Employee;

public interface EmployeeService {

	List<Employee> getEmployee();

}