package com.malganga.services;

import java.util.List;

import com.malganga.entities.Client;

public interface ClientService {

	List<Client> getClients();

}