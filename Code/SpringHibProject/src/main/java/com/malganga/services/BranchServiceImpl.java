package com.malganga.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.malganga.daos.BranchDao;
import com.malganga.entities.BranchM;
import com.malganga.entities.Client;
import com.malganga.entities.Employee;

@Transactional
@Service
public class BranchServiceImpl implements BranchService
{
	@Autowired
	BranchDao branchDao;
	
	
	public List<BranchM> getBranches()
	{
		return branchDao.select();
	}
	
	
	public List<Employee> getAllEmp(int branchId)
	{
		return branchDao.getAllEmp(branchId);
	}
	
	public List<Client> getAllClient(int branchId)
	{
		return branchDao.getAllClient(branchId);
	}
}
