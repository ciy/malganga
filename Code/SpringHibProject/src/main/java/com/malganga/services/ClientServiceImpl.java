package com.malganga.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.malganga.daos.ClientDao;
import com.malganga.daos.EmployeeDao;
import com.malganga.entities.Client;
import com.malganga.entities.Employee;

@Transactional
@Service
public class ClientServiceImpl implements ClientService
{
	@Autowired
	ClientDao clientDao;
	
	public List<Client> getClients()
	{
		return clientDao.select();
	}
}
