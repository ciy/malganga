package com.malganga.services;

import java.util.List;

import com.malganga.entities.BranchM;
import com.malganga.entities.Client;
import com.malganga.entities.Employee;

public interface BranchService 
{

	List<BranchM> getBranches();
	public List<Employee> getAllEmp(int branchId);
	public List<Client> getAllClient(int branchId);

}