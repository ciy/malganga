package com.malganga.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.malganga.daos.BranchDao;
import com.malganga.daos.ItemDao;
import com.malganga.entities.BranchM;
import com.malganga.entities.Client;
import com.malganga.entities.Employee;
import com.malganga.entities.Item;

@Transactional
@Service
public class ItemServiceImpl implements ItemService 
{
	@Autowired
	ItemDao itemDao;
	
	
	public List<Item> getItems()
	{
		return itemDao.select();
	}
	

}
