package com.malganga.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.malganga.daos.EmployeeDao;
import com.malganga.entities.Employee;

@Transactional
@Service
public class EmployeeServiceImpl implements EmployeeService 
{
	@Autowired
	EmployeeDao employeeDao;
	
	public List<Employee> getEmployee()
	{
		return employeeDao.select();
	}
}
