package com.malganga.services;

import java.util.List;

import com.malganga.entities.Item;

public interface ItemDao {

	List<Item> getItems();

}