use malganga2;

insert into Actor (actor_name,	actor_cperson,	actor_branch_fk,	actor_role,	actor_add,	actor_country,	actor_pincode,	actor_mob1,	actor_mob2,	actor_email,	actor_password) 
values("Provider1",	"providername1",	1	,	"provider", "Mumbai",	"India",	"400012", "8097505039",	"","Provider1@gmail.com","Provider1@123");

insert into Actor (actor_name,	actor_cperson,	actor_branch_fk,	actor_role,	actor_add,	actor_country,	actor_pincode,	actor_mob1,	actor_mob2,	actor_email,	actor_password) 
values	("Restaurant1",	"consumer1",	1	,	"consumer",          				"Mumbai",	"India",	"400012",
"8097499339",	"",		"Consumer1@gmail.com",	"Consumer1@123");

insert into Actor (actor_name,	actor_cperson,	actor_branch_fk,	actor_role,	actor_add,	actor_country,	actor_pincode,	actor_mob1,	actor_mob2,	actor_email,	actor_password)
values("Bearer1",	"bearer1",	1	,	"bearer",          				"Mumbai",	"India",	"400012",
"902929658	1",	"",		"Bearer1@gmail.com",	"Bearer1@123");

insert into Actor (actor_name,	actor_cperson,	actor_branch_fk,	actor_role,	actor_add,	actor_country,	actor_pincode,	actor_mob1,	actor_mob2,	actor_email,	actor_password)
values("Manager1",	"SHrikant1",	2	,	"manager",          				"Pune",	"India",	"400046",
"9869174940",	"","Manager1@gmail.com","Manager1@123");

select * from Actor;
