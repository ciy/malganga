/* Create database for system */

use malganga2;

create view Manager as 	
select actor_id_pk ,actor_cperson,actor_branch_fk,actor_role,actor_add,actor_country,actor_pincode,actor_mob1,actor_mob2,actor_email,actor_password 
from Actor 
where actor_role = 'manager';

create view Bearer as 	
select actor_id_pk ,actor_cperson,actor_branch_fk,actor_role,actor_add,actor_country,actor_pincode,actor_mob1,actor_mob2,actor_email,actor_password 
from Actor 
where actor_role = 'bearer';

create view Provider as 	
select actor_id_pk,actor_name,actor_cperson,actor_branch_fk,actor_role,actor_add,actor_country,actor_pincode,actor_mob1,actor_mob2,actor_email,actor_password 
from Actor 
where actor_role = 'provider';

create view Restaurant as 	
select actor_id_pk,actor_name,actor_cperson,actor_branch_fk,actor_role,actor_add,actor_country,actor_pincode,actor_mob1,actor_mob2,actor_email,actor_password 
from Actor 
where actor_role = 'restaurant';

show tables;
