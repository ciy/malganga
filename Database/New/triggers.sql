use malganga2;

/*
create table logTable
(	
	tablename varchar(50),
	operation varchar(100),
	time	datetime
);

*/

delimiter //
create trigger selectBranchTrigger
after select on Branch 
for each row	
begin 
insert into logTable (tablename,operation,time)
values("Branch","select",now())  
end;//

delimiter ;
