/* use database of system */

use malganga2;


/* Table relationship*/

alter table Actor 
			add constraint Actor_fk_Branch foreign key (actor_branch_fk) references Branch(branch_id_pk);

alter table Branch 
			add constraint Branch_fk_Actor foreign key (branch_head_fk) references Actor(actor_id_pk);


alter table Stock 
			add	constraint	Stock_fk_Item		foreign key(stock_item_fk) 	references Item(item_id_pk);
alter table Stock 
			add	constraint	Stock_fk_Branch		foreign key(stock_branch_fk) 	references Branch(branch_id_pk);
alter table Stock 
			add	constraint	Stock_fk_Actor		foreign key(stock_provider_fk) 	references Actor(actor_id_pk);


alter table OrderM 
			add	foreign key OrderM_fk_Actor (order_actor_fk) references Actor(actor_id_pk);


alter table Orderdetail  
			add	constraint	Orderdetail_fk_OrderM	foreign key  (orderd_oid_fk) references OrderM(order_id_pk);
alter table Orderdetail 
			add	constraint	Orderdetail_fk_Item	foreign key  (orderd_item_fk) references Item(item_id_pk);


alter table Billing  
			add	constraint	Billing_fk_OrderM	foreign key (billing_order_fk) 	references OrderM(order_id_pk);


show tables;
