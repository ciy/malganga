/* use database of system */

use malganga;


/* Table relationship*/

/* Employee table Employee
	(emp_id,emp_name,emp_role,emp_password,emp_mobile1,emp_mobile2,	emp_email,emp_branch)

-> As employees belongs to particular branch.

*/

alter table employee 
add constraint emp_fk_branch foreign key (emp_branch_fk) references branch(branch_id_pk);

/*	Branch table	

	Branch
	(branch_id,branch_add,branch_head);

	->as branch head is also an employee

*/

alter table branch 
add constraint branch_fk_emp foreign key (branch_head_fk) references employee(emp_id_pk);

/*	Client table

	Client 		
(client_id,client_name,client_cperson,client_role,client_add,client_pincode,client_country,client_mob1,client_mob2,client_email,
client_password,client_branch);

	-> home branch of client is foreign key here
*/


alter table client 
add constraint client_fk_branch foreign key (client_branch_fk) references branch(branch_id_pk);

/*	Item Table

	item(item_id,item_name,item_desc);
	
	-> Doesn't have any primary key
*/


/*	Stock Table

	stock(stock_id,stock_item,stock_quantity,stock_price,stock_branch,stock_employee,stock_provider)
	
	-> stock-item is the column refers value of item table
	-> stock-branch is the branch of company where stock is kept (in future it will be replaced by column refereing warehouse detail table
	-> stock-employee refers employee data in employee table 
	-> stock-provider refers the providers information i.e from client table data having role of provider

)
*/

alter table stock add	constraint	stock_fk_item		foreign key(stock_item_fk) 	references item(item_id_pk);
alter table stock add	constraint	stock_fk_branch		foreign key(stock_branch_fk) 	references branch(branch_id_pk);
alter table stock add	constraint	stock_fk_employee	foreign key(stock_employee_fk) 	references employee(emp_id_pk);
alter table stock add	constraint	stock_fk_client		foreign key(stock_provider_fk) 	references client(client_id_pk);


/* 	OrderMalganga Table 	

	orderM(order_id,order_customer,order_pdate,order_bdate,order_status,order_tamt,order_pamt)

	-> order_customer refers customer i.e. client table data who has ordered the material
*/

alter table orderM 
add	foreign key orderM_fk_client (order_customer_fk) references client(client_id_pk);


/* 	Order Detail 	
	
	Orderdetail(orderd_id,orderd_oid,orderd_item,orderd_quantity,orderd_itmprice)

	-> orderd_oid refers order data which is inititated at the time of adding order
	-> orderd_item refers data of item table

*/

alter table orderdetail  add	constraint	orderdetail_fk_order	foreign key  (orderd_oid_fk) references orderM(order_id_pk);
alter table orderdetail  add	constraint	orderdetail_fk_item	foreign key  (orderd_item_fk) references item(item_id_pk);


show tables;
