use malganga;

insert into client	(client_name,	client_cperson,	client_role,	client_add,	client_pincode,	client_country,	client_mob1,
			client_mob2,	client_email,	client_password,	client_branch_fk) 
values			("restaurant1",	"client1",	"provider",	"Mumbai",	"400012",	"India",	"8097505039",
			"8097499339",	"client1@email",	"Client1@123",	1),
			("restaurant2",	"client2",	"consumer",	"Mumbai",	"400012",	"India",	"",
			"",		"client2@email",	"Client2@123",	1),
			("restaurant3",	"client3",	"provider",	"Pune",		"400042",	"India",	"12",
			"",		"client3@email",	"Client3@123",	2);

select * from client;
