use malganga;

insert into employee	(emp_id_pk,	emp_name,	emp_role,	emp_password,	emp_mobile1,	emp_mobile2,	emp_email,	emp_branch_fk) 
values			("emp1",	"Sumeet",	"bearer",	"Sumeet@123",	"8097499339",	"",	"Sumeet@gmail.com",	1),
			("emp2",	"Pankaj",	"bearer",	"Pankaj@123",	"8097505039",	"",	"Pankaj@gmail.com",	1),
			("emp3",	"Shrikant",	"manager",	"Shrikant@123",	"9029012930",	"",	"Shrikant@gmail.com",	2);	

select * from employee;
